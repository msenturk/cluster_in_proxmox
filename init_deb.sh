#!/bin/bash
_init_deb(){
    SECONDS=0

    declare -g vm_ID="998"                      # Virtual machine ID
    declare -g vm_NAME="debian-skel"            # Virtual machine name
    declare -g vm_HNAME="debian-test"           # Virtual machine hostname
    declare -g vm_RELEASE="buster"              # Debian release code name (buster, bullseye)

    declare -g vm_SIZE="64"                     # Virtual machine qcow2 disk size (G)
    declare -g vm_RAM="2048"                    # Virtual machine RAM size (G)
    declare -g vm_CORE="4"                      # Virtual machine cpu core count
    declare -g vm_NET="vmbr2"                   # Virtual machine network interface

    declare -g root_pass=''                     # If you leave blank, the password will be created.
    declare -g new_user_pass=''                 # If you leave blank, the password will be created.

    declare -ag VM_IDS=(100 101 102 103 104)    # These VMs will be created (ID range: 100..254)

    # Generate MAC address
    declare -g mac_addr="$(od -An -N6 -tx1 /dev/urandom | sed -e 's/^  *//' -e 's/  */:/g' -e 's/:$//' -e 's/^\(.\)[13579bdf]/\10/')"

    # Generate root password
    test -z "${root_pass}" \
        && declare -g root_pass="$(tr -dc [:alnum:] < /dev/urandom | head -c 10)"

    # Generate new_user password
    test -z "${new_user_pass}" \
        && declare -g new_user_pass="$(tr -dc [:alnum:] < /dev/urandom | head -c 10)"

    _create_debian_image(){
        echo -e "\nDebootstrapping ${vm_ID} ...\n"

        vmdebootstrap \
        --distribution="${vm_RELEASE}" \
        --log="/tmp/bootstrap.log" \
        --verbose \
        --size ${vm_SIZE}GiB \
        --sparse \
        --grub \
        --no-extlinux \
        --root-password="${root_pass}" \
        --user="new_user/${new_user_pass}" \
        --sudo \
        --convert-qcow2 \
        --hostname ${vm_HNAME} \
        --enable-dhcp \
        --image /var/lib/vz/images/${vm_ID}/vm-${vm_ID}-disk-0.qcow2 \
        --package openssh-server \
        --package curl && \
            echo -e "
            \nBootstrap succeed!
            \tVirtual machine ID:\t${vm_ID}
            \tVirtual machine name:\t${vm_NAME}
            \tVirtual machine MAC:\t${mac_addr}
            \tDisk usage:\t\t$(du -sh /var/lib/vz/images/${vm_ID}/vm-${vm_ID}-disk-0.qcow2 | awk '{print $1}')
            \n\tRoot password:\t\t${root_pass}
            \tnew_user password:\t${new_user_pass}
            \nProcess completed in $((SECONDS/60)) min $((SECONDS%60)) sec\n
            " | \
                sed 's/    //g' || \
                { echo -e "\nBootstrap failed! \n" && tail /tmp/bootstrap.log && return || exit; }
    }

    _create_vm(){
        echo -e "\nCreating ${vm_ID} ...\n"

        qm create ${vm_ID} \
            --name ${vm_NAME} \
            --net0 virtio="${mac_addr}",bridge=${vm_NET},firewall=1 \
            --virtio0 local:${vm_ID}/vm-${vm_ID}-disk-0.qcow2,size=${vm_SIZE}G \
            --bootdisk virtio0 \
            --ostype l26 \
            --memory ${vm_RAM} \
            --onboot no \
            --sockets 1 \
            --cores ${vm_CORE} \
            --scsihw virtio-scsi-pci
    }

    _clone_vm(){
        echo -e "\n${vm_ID} is clonning ...\n"

        for i in ${VM_IDS[@]}
        do
            qm clone ${vm_ID} ${i} \
                --description vm${i} \
                --format qcow2 \
                --full true \
                --name vm${i} \
                --storage second-disk 1>/tmp/pers && warn "${i} is ready"
        done
    }

    _set_static_IP(){
        echo -e "\nSet dhcp-static IP configuration\n"

        # Purge DNSmasq config
        sed '/^dhcp-host.*/d' -i /etc/dnsmasq.conf
        rm -f                    /var/lib/misc/dnsmasq.leases

        # Edit dnsmasq.conf
        for i in ${VM_IDS[@]}
        do
            current_MAC="$(awk -F'[=,]' '/^net0/{print $2}' /etc/pve/qemu-server/${i}.conf)"
            current_IP="$(ip r s dev ${vm_NET} | awk -v _id=${i} -F\. '{print $1"."$2"."$3"."_id}')"

            echo "dhcp-host=${current_MAC},${current_IP}" >> /etc/dnsmasq.conf && \
            warn "${current_IP} appended to dnsmasq.conf"
        done
    }

    _start_vms(){
        echo -e "\nStart VMs\n"

        for i in ${VM_IDS[@]}
        do
            qm start ${i} && warn "${i} started"
            sleep 0.5
        done
    }

    mkdir -p /var/lib/vz/images/${vm_ID}
    touch    /tmp/bootstrap.log

    # Check disk
    test -e "/var/lib/vz/images/${vm_ID}/vm-${vm_ID}-disk-0.qcow2"  && \
        { echo -e "\n ${vm_ID} is already exists\n"      && return || exit; }

    # Check VM_ID
    qm list | awk '{print $1}' | grep ${vm_ID}                      && \
        { echo -e "\n ${vm_ID} vmID is already in use\n" && return || exit; }

    clear

    _create_debian_image                && \
    _create_vm                          && \
    _clone_vm                           && \
    _set_static_IP                      && \
     systemctl restart dnsmasq.service  && \
    _start_vms

};

_init_deb
